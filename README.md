# README #

This program is convert SAS Xpt file to sas program include datalines and data.

### How do I get set up? ###

* Download XPT2SAS.exe in src/Win32 or Win64
* This is for Windows 32/64bit only.

### How to use ###

* Start XPT2SAS.exe
* Specify SAS XPT file path to XPT file box.
* Specify SAS program file path to SAS program box.
* click convert.

### Who do I talk to? ###

If you encounter any errors when running the program, please contact me.
https://communities.sas.com/t5/user/viewprofilepage/user-id/226565

The source code will be made available in the future.